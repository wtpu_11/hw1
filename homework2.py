import random
import time


def matrix_mul(m1, m2):
    result = list()

    # The multiplication result of 2 matrices of total row and total column
    rows = len(m2[0])
    cols = len(m1)
    count = len(m2)
    row = 0

    while row < rows:
        result.append(list())
        col = 0
        while col < cols:
            index = 0
            value = 0
            while index < count:
                value += (m1[row][index] * m2[index][col])
                index += 1
            result[row].append(value)
            col += 1
        row += 1

    return result


if __name__ == '__main__':

    # use integer
    A = []
    for i in range(300):
        A.append([])
        for j in range(200):
            A[i].append(random.randint(1, 1000))

    B = []
    for i in range(200):
        B.append([])
        for j in range(300):
            B[i].append(random.randint(1, 1000))
    t = time.time()
    begin_time = int(round(t * 1000))
    C = matrix_mul(A, B)
    t = time.time()
    print('integer data use time:{} ms'.format(int(round(t * 1000) - begin_time)))

    # use double
    A = []
    for i in range(300):
        A.append([])
        for j in range(200):
            A[i].append(random.random())

    B = []
    for i in range(200):
        B.append([])
        for j in range(300):
            B[i].append(random.random())
    t = time.time()
    begin_time = int(round(t * 1000))
    C = matrix_mul(A, B)
    t = time.time()
    print('double data use time:{} ms'.format(int(round(t * 1000) - begin_time)))

    # use integer
    A = []
    for i in range(300):
        A.append([])
        for j in range(200):
            A[i].append(random.randint(1, 1000))

    B = []
    for i in range(200):
        B.append([])
        for j in range(300):
            B[i].append(random.randint(1, 1000))
    t = time.time()
    begin_time = int(round(t * 1000))
    C = matrix_mul(B, A)
    t = time.time()
    print('integer data use time:{} ms'.format(int(round(t * 1000) - begin_time)))

    # use float
    A = []
    for i in range(300):
        A.append([])
        for j in range(200):
            A[i].append(random.random())

    B = []
    for i in range(200):
        B.append([])
        for j in range(300):
            B[i].append(random.random())
    t = time.time()
    begin_time = int(round(t * 1000))
    C = matrix_mul(B, A)
    t = time.time()
    print('double data use time:{} ms'.format(int(round(t * 1000) - begin_time)))

