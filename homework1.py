
def analysis(filename):
    # key = address  value = [read, write, instruction fetch, count]
    result = dict()
    with open(filename, 'r') as f:
        for line in f:
            line = line.strip()
            items = line.split(' ')
            key = items[1]
            operate_index = int(items[0])
            if items[1] not in result:
                result[key] = [0, 0, 0, 1]
            else:
                result[key][3] += 1

            result[key][operate_index] += 1

    return result

if __name__ == '__main__':
    r = analysis('cc1.din')
    for key in r:
        print('{} : read:{} write:{} instruction fetch:{} count:{}'.format(key, r[key][0], r[key][1], r[key][2],
                                                                           r[key][3]))
    print('===============================================================================')
    r = analysis('tex.din')
    i = 0
    for key in r:
        i += 1
        print('{} : read:{} write:{} instruction fetch:{} count:{}'.format(int(key,16), r[key][0], r[key][1], r[key][2], r[key][3]))
        if i > 200:
            break
